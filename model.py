#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Conv2D, MaxPool2D, Flatten
from tensorflow.keras.callbacks import TensorBoard

IMG_SIZE = 256

def model_zero(loss='categorical_crossentropy', opt='adam'):
    """
    This function implements a small example model that you can use/modify.
    You are not limited to creating your own model, you can use already
    implemented models such as ResNets etc.
    """
    # building a linear stack of layers with the sequential model
    model = Sequential()
    # convolutional layer 
    model.add(Conv2D(36, kernel_size=(3,3), strides=(1,1), padding='valid', activation='relu', input_shape=(IMG_SIZE,IMG_SIZE,1)))
    model.add(MaxPool2D(pool_size=(1,1)))
    # flatten output of conv
    model.add(Flatten())
    # hidden layer
    model.add(Dense(128, activation='relu'))
    # output layer
    model.add(Dense(3, activation='softmax'))

    # compiling the sequential model
    model.compile(loss=loss, metrics=['accuracy'], optimizer=opt)

    return model


def model_one(loss='categorical_crossentropy', opt='adam'):
    """
    A quick model used to test Keras Sequential Model Parameters
    """
    # building a linear stack of layers with the sequential model
    model = Sequential()
    # convolutional layer 
    model.add(Conv2D(25, kernel_size=(3,3), strides=(1,1), padding='valid', activation='relu', input_shape=(28,28,1)))
    model.add(MaxPool2D(pool_size=(1,1)))
    # flatten output of conv (input) layer 
    model.add(Flatten())

    # hidden layer
    model.add(Dense(100, activation='relu'))
    model.add(Dropoutx=0.5)
    # hidden layer
    model.add(Dense(100, activation='relu'))
    model.add(Dropoutx=0.25)

    # output layer
    model.add(Dense(10, activation='softmax'))

    # compiling the sequential model
    model.compile(loss=loss, metrics=['accuracy'], optimizer=opt)

    return model

def model_test(dense, layer, conv, loss='categorical_crossentropy', opt='adam'):
    """
    A skeleton model that can be used to iterate over different hyperparameters to the model.
    """
    # building a linear stack of layers with the sequential model
    model = Sequential()
    # convolutional layer 
    model.add(Conv2D(25, kernel_size=(3,3), strides=(1,1), padding='valid', activation='relu', input_shape=(256,256,1)))
    model.add(MaxPool2D(pool_size=(1,1)))
    # flatten output of conv (input) layer 
    model.add(Flatten())

    for i in range(conv-1):
        model.add(Conv2D(layer, kernel_size=(3,3), strides=(1,1), padding='valid', activation='relu'))
        model.add(MaxPool2D(pool_size=(1,1)))
    model.add(Flatten())

    # Hidden/dense layer
    for i in range(dense):
        model.add(Dense(layer, activation='relu'))

    # Final layer
    model.add(Dense(layer, activation='relu'))

    # output layer
    model.add(Dense(3, activation='softmax'))

    # compiling the sequential model
    model.compile(loss=loss, metrics=['accuracy'], optimizer=opt)

    return model

def model_opt(loss='categorical_crossentropy', opt='adam'):
    """
    The optimised model with the dataset provided by HeartLab as of August 2021.
    """
    model = Sequential()
    # Convolutional/Input Layer 
    model.add(Conv2D(25, kernel_size=(3,3), strides=(1,1), padding='valid', activation='relu', input_shape=(256,256,1)))
    model.add(MaxPool2D(pool_size=(1,1)))
    # flatten output of conv (input) layer 
    model.add(Flatten())

    # Variable number of Dense Layers
    model.add(Dense(256, activation='relu'))
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.2))

    # Final layer
    model.add(Dense(256, activation='relu'))

    # output layer
    model.add(Dense(3, activation='softmax'))

    # compiling the sequential model
    model.compile(loss='categorical_crossentropy', metrics=['accuracy'], optimizer='adam')
    return model